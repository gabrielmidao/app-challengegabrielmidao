export default [{
    name: 'preview',
    subTituloP: 'NEW WAVE',
    image: require('../../assets/images/imagemF.png'),
    key: '1',
  }, 
  {
    name: 'PREVIEW',
    image: require('../../assets/images/imagemH.png'),
    key: '2',
  }, 
  {
    name: 'HOT TRENDS',
    image: require('../../assets/images/ImagemG2.png'),
    key: '3',
  },
  {
    name: 'JAQUETAS',
    image: require('../../assets/images/imagemI2.png'),
    key: '4',
  },

  {
    nameT: 'VISTOS RECENTEMENTE:',
    subTituloP: 'Camisa \nde seda parka',
    subTituloS: 'R$ 529',
    subTituleT: '5x de R$ 105,80',
    iconTitleP: 'Encontre uma loja',
    iconTitleS: 'Fale com um vendedor',
    imageT: require('../../assets/images/imagemJ.png'),
    key: '5',
  },
  {
    nameT: 'VISTOS RECENTEMENTE:',
    subTituloP: 'Camisa \nde seda parka',
    subTituloS: 'R$ 529',
    subTituleT: '5x de R$ 105,80',
    iconTitleP: 'Encontre uma loja',
    iconTitleS: 'Fale com um vendedor',
    imageT: require('../../assets/images/imagemK.png'),
    key: '6',
  },
  
  {
    nameL: 'VOCÊ PODE CURTIR: ',
    subTituloP: 'Camisa \nde seda parka',
    subTituloS: 'R$ 529',
    subTituleT: '5x de R$ 105,80',
    iconTitleP: 'Encontre uma loja',
    iconTitleS: 'Fale com um vendedor',
    imageT: require('../../assets/images/imagemL.png'),
    key: '7',
  },
  {
    nameZ: 'VOCÊ PODE CURTIR: ',
    subTituloPZ: 'Camisa \nde seda parka',
    subTituloSZ: 'R$ 529',
    subTituleTZ: '5x de R$ 105,80',
    iconTitleP: 'Encontre uma loja',
    iconTitleS: 'Fale com um vendedor',
    imageZ: require('../../assets/images/imagemZ.png'),
    key: '8',
  },
 
];