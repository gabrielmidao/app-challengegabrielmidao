import { StyleSheet } from 'react-native';

const stylesBTN = StyleSheet.create({
    container: {
        width: '50%',
        padding: 10,
        marginLeft: '20%',
        position: 'absolute',
        bottom: 140,
        
    },
    button: {
        backgroundColor: 'transparent',
        borderWidth: 1,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        borderColor: 'white',
       
    },
    text: {
        color: 'white',
        fontWeight: '500',
        fontSize: 12,
        textTransform: 'uppercase',
        fontFamily: 'serif',
    },
});

export default stylesBTN;