import React from 'react';
import { AntDesign } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons'; 
import{View,Text, ImageBackground, Pressable, Image} from 'react-native';
import Dash from 'react-native-dash';
import styles from './styles';
import { useNavigation } from '@react-navigation/native';
import stylesBTN from './stylesBTN';


function BotaoInicial({}) {
const navigation = useNavigation();
return(
<View style={stylesBTN.container}>
<Pressable
  //onPress={() => navigation.navigate('MenuPrincipal')}
  onPress={() => console.warn('Oi')}
  style={stylesBTN.button}
>
 <Text style={stylesBTN.text}>Entrar</Text>
</Pressable>
</View>
)}



const ModeloItem = (props) => {

    const {name, nameT,nameL,nameZ, subTituloP,subTituloS,subTituleT,subTituloPZ, 
      subTituloSZ, subTituleTZ, image,imageT,imageZ, iconTitleP,iconTitleS, key} = props.teste;

    function TesteKey(){
      if(key == "5"){
      return(
        <View style={styles.containerAlt} backgroundColor={"#dc885c"} >
       
        <ImageBackground source={imageT} 
        style={styles.imageT} />
        <View style={styles.titlesT}>
          <Text style={styles.titleT}>{nameT}</Text>
          <Text style={styles.subtitleT}>{subTituloP}</Text>
          <Text style={styles.subTituloS}>{subTituloS}</Text>
          <Text style={styles.subTituleT}>{subTituleT}</Text>
          <Dash style={styles.dash} dashColor='black'  dashGap={5} dashLength={166} dashThickness={1} ></Dash>
            <AntDesign style={styles.iconP} name="enviromento" size={15} />
            <Text style={styles.iconTitleP} >{iconTitleP}</Text>
            <Dash style={styles.dash} dashColor='black'  dashGap={5} dashLength={166} dashThickness={1} ></Dash>
            <Entypo style={styles.iconS} name="chat"  color="black"  size={15}/>
            <Text style={styles.iconTitleS} >{iconTitleS}</Text>
            <Dash style={styles.dash} dashColor='black'  dashGap={5} dashLength={166} dashThickness={1} ></Dash>
        </View>
      
      </View>
        
      )
      } else if(key == '6'){
        return(
          <View style={styles.containerAlt} backgroundColor={"#e4a783"}>
          <ImageBackground source={imageT} 
          style={styles.imageT} />
          <View style={styles.titlesT}>
            <Text style={styles.titleT}>{nameT}</Text>
            <Text style={styles.subtitleT}>{subTituloP}</Text>
            <Text style={styles.subTituloS}>{subTituloS}</Text>
            <Text style={styles.subTituleT}>{subTituleT}</Text>
            <Dash style={styles.dash} dashColor='black'  dashGap={5} dashLength={166} dashThickness={1} ></Dash>
            <AntDesign style={styles.iconP} name="enviromento" size={15} />
            <Text style={styles.iconTitleP} >{iconTitleP}</Text>
            <Dash style={styles.dash} dashColor='black'  dashGap={5} dashLength={166} dashThickness={1} ></Dash>
            <Entypo style={styles.iconS} name="chat"  color="black"  size={15}/>
            <Text style={styles.iconTitleS} >{iconTitleS}</Text>
            <Dash style={styles.dash} dashColor='black'  dashGap={5} dashLength={166} dashThickness={1} ></Dash>
          </View>
        
        </View>
        )
      }else if(key == '7'){
        
        return(
          <View style={styles.containerAlt} backgroundColor={'#beb4ac'} >
          <ImageBackground source={imageT} 
          style={styles.imageT} />
          <View style={styles.titlesT}>
            <Text style={styles.titleL}>{nameL}</Text>
            <Text style={styles.subtitleT}>{subTituloP}</Text>
            <Text style={styles.subTituloS}>{subTituloS}</Text>
            <Text style={styles.subTituleT}>{subTituleT}</Text>
            <Dash style={styles.dash} dashColor='black'  dashGap={5} dashLength={166} dashThickness={1} ></Dash>
            <AntDesign style={styles.iconP} name="enviromento" size={15} />
            <Text style={styles.iconTitleP} >{iconTitleP}</Text>
            <Dash style={styles.dash} dashColor='black'  dashGap={5} dashLength={166} dashThickness={1} ></Dash>
            <Entypo style={styles.iconS} name="chat"  color="black"  size={15}/>
            <Text style={styles.iconTitleS} >{iconTitleS}</Text>
            <Dash style={styles.dash} dashColor='black'  dashGap={5} dashLength={166} dashThickness={1} ></Dash>
          </View>
        
        </View>
          
        )
        
      }else if(key == '8'){
        
        return(
          <View style={styles.containerAlt} backgroundColor={'#f05541'} >
          <ImageBackground source={imageZ} 
          style={styles.imageZ} />
          <View style={styles.titlesT}>
            <Text style={styles.titleZ}>{nameZ}</Text>
            <Text style={styles.subTitlePZ}>{subTituloPZ}</Text>
            <Text style={styles.subTitleSZ}>{subTituloSZ}</Text>
            <Text style={styles.subTitleTZ}>{subTituleTZ}</Text>
            <Dash style={styles.dash} dashColor='black'  dashGap={5} dashLength={166} dashThickness={1} ></Dash>
            <AntDesign style={styles.iconP} name="enviromento" size={15} />
            <Text style={styles.iconTitleP} >{iconTitleP}</Text>
            <Dash style={styles.dash} dashColor='black'  dashGap={5} dashLength={166} dashThickness={1} ></Dash>
            <Entypo style={styles.iconS} name="chat"  color="black"  size={15}/>
            <Text style={styles.iconTitleS} >{iconTitleS}</Text>
            <Dash style={styles.dash} dashColor='black'  dashGap={5} dashLength={166} dashThickness={1} ></Dash>
          </View>
          
        </View>
          
        )
        
      }else {
        return(
          <View style={styles.frontContainer}>
          <ImageBackground source={image} 
          style={styles.image} />
          <View style={styles.titles}>
            <Text style={styles.title}>{name}</Text>
            <Text style={styles.subtitle}>{subTituloP}</Text>
            
          </View>
          <BotaoInicial />
    
        </View>
        )
      }
    }
 
    return(
        
      <TesteKey />

    );
};


export default ModeloItem;