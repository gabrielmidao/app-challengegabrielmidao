
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs' 
import ModelosLista from './Componentes/FrontLista/index';
import MenuPrincipal from './Componentes/Telas/MenuPrincipal';
import Icon from 'react-native-vector-icons/Feather';


const Tab = createBottomTabNavigator();


export default function App() {

    return (
      <NavigationContainer>
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ color, size }) => {
              let iconName;

              switch(route.name) {
                case 'Inicio':
                  iconName = 'home';
                  break;
                case 'Menu':
                  iconName = 'list';
                  break;
                case 'Minha lista':
                  iconName = 'bookmark';
                  break;
                case 'Conta':
                  iconName = 'user';
                  break;
              }

              return <Icon name={iconName} size={size} color={color} />;
            },
          })}
            tabBarOptions={{
              activeTintColor: 'red',
              inactiveTintColor: '#777',
            }}
        
        
        >
          <Tab.Screen name="Inicio" component={ModelosLista} />
          <Tab.Screen name="Menu" component={MenuPrincipal} />
          <Tab.Screen name="Minha lista" component={MenuPrincipal} />
          <Tab.Screen name="Conta" component={MenuPrincipal} />
        </Tab.Navigator>
      </NavigationContainer>
    );
  
  
  
}



  

